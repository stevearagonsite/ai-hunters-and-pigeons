﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionNode : Node{

    public Questions question;
    public Dictionary<Questions, Func<NPCHunter, bool>> desicionTable = new Dictionary<Questions, Func<NPCHunter, bool>>();

    Func<NPCHunter, bool> hasWeapon = x => x.currentWeapon;
    Func<NPCHunter, bool> hasBullets = x => x.bullets > 0;
    Func<NPCHunter, bool> isLoaded = x => x.currentWeapon.loadedBullet;
    Func<NPCHunter, bool> iSeePiegons = x => x.lineOfSight.TargetInSight;
    Func<NPCHunter, bool> canIShoot = x => x.lineOfSight.TargetInShoot;

    public Node trueNode;
    public Node falseNode;

    public enum Questions { HasWeapon, HasBullets, IsLoaded, ISeePigeons, CanIShoot }

    void Awake(){
        //Save questions.
        desicionTable.Add(Questions.HasWeapon, hasWeapon);
        desicionTable.Add(Questions.HasBullets, hasBullets);
        desicionTable.Add(Questions.IsLoaded, isLoaded);
        desicionTable.Add(Questions.ISeePigeons, iSeePiegons);
        desicionTable.Add(Questions.CanIShoot, canIShoot);
    }

    public override void Execute(NPCHunter reference){
        foreach (var desicion in desicionTable){
            if (desicion.Key == question){
                (desicion.Value(reference) ? trueNode : falseNode).Execute(reference);
            }
        }
    }

    void OnDrawGizmos(){
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, trueNode.transform.position);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, falseNode.transform.position);
    }
}
