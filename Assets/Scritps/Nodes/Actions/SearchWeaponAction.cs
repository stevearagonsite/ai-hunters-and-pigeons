﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchWeaponAction : ActionNode {
    public override void Execute(NPCHunter reference){
        Debug.Log("Search Weapon");
        reference.sm.SetState<StateGetWeapon>();
    }
}
