﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAction : ActionNode {

    public override void Execute(NPCHunter reference){
        Debug.Log("Shoot");
        reference.sm.SetState<StateShoot>();
    }
}
