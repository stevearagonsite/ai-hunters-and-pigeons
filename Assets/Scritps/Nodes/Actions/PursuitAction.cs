﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PursuitAction : ActionNode {
    public override void Execute(NPCHunter reference){
        Debug.Log("Pursuit");
        reference.sm.SetState<StatePursuit>();
    }
}
