﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadAction : ActionNode {
    public override void Execute(NPCHunter reference){
        Debug.Log("Reload");
        reference.sm.SetState<StateReload>();
    }
}
