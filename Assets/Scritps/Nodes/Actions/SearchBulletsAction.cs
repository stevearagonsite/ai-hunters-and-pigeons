﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchBulletsAction : ActionNode {
    public override void Execute(NPCHunter reference){
        Debug.Log("Search Bullets");
        reference.sm.SetState<StateGetBullets>();
    }
}
