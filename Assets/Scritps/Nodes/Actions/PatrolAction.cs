﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolAction : ActionNode {
    public override void Execute(NPCHunter reference){
        Debug.Log("Patrol");
        reference.sm.SetState<StatePatrol>();
    }
}
