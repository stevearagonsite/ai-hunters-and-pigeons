﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoReference : MonoBehaviour {

    NPCPiegon[] _allPiegons;
    NPCHunter[] _allHunters;
    PackBullets[] _allBullets;
    NPCPiegonBoss _PiegonBoss;
    
    public static bool havePiegeonsBoss;

    void Start() {
        _allPiegons = GameObject.FindObjectsOfType<NPCPiegon>();
        if (_allPiegons != null && _allPiegons.Length > 0){
            var x = Random.Range(0, _allPiegons.Length - 1);
            _allPiegons[x].gameObject.GetComponent<NPCPiegonBoss>().enabled = true;
            _PiegonBoss = _allPiegons[x].gameObject.GetComponent<NPCPiegonBoss>();
        }
    }
	
	void Update () {

        _allPiegons = GameObject.FindObjectsOfType<NPCPiegon>();
        if (_PiegonBoss == null && _allPiegons.Length > 0) {
            var x = Random.Range(0, _allPiegons.Length - 1);
            _allPiegons[x].gameObject.GetComponent<NPCPiegonBoss>().enabled = true;
            _PiegonBoss = _allPiegons[x].gameObject.GetComponent<NPCPiegonBoss>();
        }
    }
    
}
