﻿using UnityEngine;
using System.Collections;

public class LineOfSight : MonoBehaviour {
    public Transform target;
    public float viewAngle;
    public float viewDistance;

    const int wallLayer = 9;
    bool _targetInSight;
    bool targetInShoot;
    int layerMask;

    public Transform Target { get {return target != null ? target.transform : transform; } }
    public bool TargetInSight {get { return _targetInSight; } }
    public bool TargetInShoot { get { return targetInShoot; } }

    public void Configure(float viewAngle, float viewDistance){
        this.viewAngle = viewAngle;
        this.viewDistance = viewDistance;
    }
	
	void Awake () {
        layerMask = (1<<wallLayer);
	}

    void Update() {
        if (target != null){
            var dirToTarget = target.transform.position - transform.position;
            var angleToTarget = Vector3.Angle(transform.forward, dirToTarget);
            var sqrDistanceToTarget = (transform.position - target.transform.position).sqrMagnitude;

            RaycastHit rch;

            _targetInSight =
                angleToTarget <= viewAngle &&
                sqrDistanceToTarget <= viewDistance * viewDistance &&
                !Physics.Raycast(
                    transform.position,
                    dirToTarget,
                    out rch,
                    Mathf.Sqrt(sqrDistanceToTarget),
                    layerMask
                    );

            targetInShoot =
                angleToTarget <= viewAngle &&
                sqrDistanceToTarget <= (viewDistance * viewDistance) / 2 &&
                !Physics.Raycast(
                    transform.position,
                    dirToTarget,
                    out rch,
                    Mathf.Sqrt(sqrDistanceToTarget),
                    layerMask
                    );
        }
        else
        {
            _targetInSight = false;
            targetInShoot = false;
        }
    }

    void OnDrawGizmos(){
        if (target != null){
            Gizmos.color = _targetInSight ? Color.green : Color.red;
            Gizmos.DrawLine(transform.position, target.transform.position);
        }

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position+ (transform.forward * viewDistance));

        Vector3 rightLimit = Quaternion.AngleAxis(viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (rightLimit * viewDistance));

        Vector3 leftLimit = Quaternion.AngleAxis(-viewAngle,transform.up)* transform.forward;
        Gizmos.DrawLine(transform.position,transform.position + (leftLimit * viewDistance));

        Gizmos.DrawWireSphere(transform.position,viewDistance);
    }
}
