﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StatePursuit : State{

    Transform _target;
    NavMeshAgent _agent;
    public StatePursuit(StateMachine sm, NPCBase n) : base(sm, n){}

    public override void Awake() {
        _agent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
        _target = ((NPCHunter)npc).target;
    }

    public override void Execute(){
        if (_target)
            _agent.SetDestination(_target.transform.position);
    }

    public override void Sleep(){
    }
}
