﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateReload : State{

    NPCHunter _reference;

    public StateReload(StateMachine sm, NPCBase n) : base(sm, n){}

    public override void Awake(){
        _reference = ((NPCHunter)npc);
    }

    public override void Execute(){
        if (_reference.bullets > 0){
            if (_reference.bullets < 5){
                _reference.currentWeapon.ReloadWeapon(_reference.bullets);
            }else {
                _reference.currentWeapon.ReloadWeapon(5);
            }
        }
    }

    public override void Sleep(){
    }
}
