﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateGetBullets : State {

    Transform _target;
    PackBullets[] _allPackBullets;
    NavMeshAgent _agent;

    public StateGetBullets(StateMachine sm, NPCBase n) : base(sm, n){}

    public override void Awake(){
        _allPackBullets = GameObject.FindObjectsOfType<PackBullets>();
        _target = SearchTarget(npc.transform, _allPackBullets);
        _agent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    Transform SearchTarget(Transform currentPosition, PackBullets[] S)
    {
        if (currentPosition && S.Length > 0)
        {

            var x = Vector3.Distance(currentPosition.position, S[0].transform.position);
            var value = S[0];

            foreach (var element in S){
                var y = Vector3.Distance(currentPosition.position, element.transform.position);

                if (x > y){
                    x = y;
                    value = element;
                }
            }
            
            return value.transform;
        }
            //throw new System.Exception("Parameter cannot be null");
        return null;
    }

    public override void Execute(){
        if (_target) _agent.SetDestination(_target.transform.position);
    }

    public override void Sleep(){
        _target = null;
    }
}
