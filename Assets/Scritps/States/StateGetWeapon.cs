﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateGetWeapon : State{

    Transform _target;
    Weapon[] _allWeapons;
    NavMeshAgent _agent;

    public StateGetWeapon(StateMachine sm, NPCBase n) : base(sm, n){}

    public override void Awake(){
        _allWeapons = GameObject.FindObjectsOfType<Weapon>();
        _target = SearchTarget( npc.transform , _allWeapons );
        _agent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    Transform SearchTarget(Transform currentPosition, Weapon[] S)
    {
        if (currentPosition && S.Length > 0)
        {

            var x = Vector3.Distance(currentPosition.position, S[0].transform.position);
            var value = S[0];

            foreach (var element in S)
            {
                var y = Vector3.Distance(currentPosition.position, element.transform.position);

                if (x > y && !element.haveBoss)
                {
                    x = y;
                    value = element;
                }
            }

            return value.transform;
        }
            //throw new System.Exception("Parameter cannot be null");
        return null;
    }

    public override void Execute(){
        if (_target) _agent.SetDestination(_target.transform.position);
    }

    public override void Sleep(){
        _target = null;
    }
}
