﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateShoot : State{

    NPCHunter _reference;

    public StateShoot(StateMachine sm, NPCBase n) : base(sm, n){}

    public override void Awake(){
        _reference = ((NPCHunter)npc);
    }

    public override void Execute(){
        if (_reference.currentWeapon.ammo > 0){
            _reference.currentWeapon.Shoot(_reference.target);
        }
    }

    public override void Sleep(){
    }
}
