﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StatePatrol : State{

    ZoneScript[] _allzones;
    ZoneScript _target;
    NavMeshAgent _agent;


    public StatePatrol(StateMachine sm, NPCBase n) : base(sm, n){
    }

    public override void Awake(){
        _allzones = GameObject.FindObjectsOfType<ZoneScript>();
        SearchZone();
        _agent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void SearchZone(){

        if (_allzones.Length > 0){
            int i = UnityEngine.Random.Range(0, _allzones.Length - 1);
            _target = _allzones[i];
        }
    }

    public override void Execute(){
        if (_target){
            _agent.SetDestination(_target.transform.position);
            var distance = Vector3.Distance(npc.transform.position, _target.transform.position);
            if (distance < 10)
                SearchZone();
        }
    }

    public override void Sleep(){
        _target = null;
    }
}
