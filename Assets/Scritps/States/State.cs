﻿using UnityEngine;
using System.Collections;

public abstract class State{

    StateMachine _sm;
    public NPCBase npc;

    /// <summary>
    /// Create the state.
    /// </summary>
    /// <param name="sm">The state machine catch the state.</param>
    public State(StateMachine sm, NPCBase n){
        _sm = sm;
        npc = n;
    }

    /// <summary>
    /// This method start when the state begin.
    /// </summary>
    public abstract void Awake();

    /// <summary>
    /// This method start when the state conclude.
    /// </summary>
    public abstract void Sleep();

    /// <summary>
    /// This method call constantly while in the state.
    /// </summary>
    public abstract void Execute();

}
