﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StateMachine {

    State _currentState;
    List<State> _states = new List<State>();

    /// <summary>
    /// Call the execute of the current state.
    /// </summary>
	public void Update() {

        if (_currentState != null) _currentState.Execute();
    }

    /// <summary>
    /// Add the state.
    /// </summary>
    /// <param name="s">the state to add.</param>
    public void AddState(State s) {

        _states.Add(s);
        if (_currentState == null)
            _currentState = s;
    }

    /// <summary>
    /// Change the state.
    /// </summary>
    /// <param name="s">Type of state.</param>
    public void SetState<T>() where T : State {

        for (int i = 0; i < _states.Count; i++) {
            if (_states[i].GetType() == typeof(T)) {
                _currentState.Sleep();
                _currentState = _states[i];
                _currentState.Awake();
            }
        }
    }

    /// <summary>
    /// if is current state
    /// </summary>
    /// <typeparam name="T">State script</typeparam>
    /// <returns>True or false</returns>
    public bool IsActualState<T>() where T : State {
        return _currentState.GetType() == typeof(T);
    }

    /// <summary>
    /// Search by index of its type.
    /// </summary>
    /// <param name="t">Type of state.</param>
    /// <returns>Return the index.</returns>
    private int SearchState(Type t){

        int ammountOfStates = _states.Count;
        for (int i = 0; i < ammountOfStates; i++)
            if (_states[i].GetType() == t)
                return i;
        return -1;
    }
}
