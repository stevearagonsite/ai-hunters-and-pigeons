﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCPiegonBoss : MonoBehaviour {

    public Material bossMaterial;
    ZoneScript _target;
    ZoneScript[] _allzones;
    NPCHunter[] _allHunters;
    NavMeshAgent agent;

    void Start(){

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        agent.stoppingDistance = 3;
        agent.speed = 5;
        agent.acceleration = 15;

        Debug.Log("New boss");

        GetComponent<NPCPiegon>().enabled = false;
        NPCPiegon.target = this.gameObject;
        InfoReference.havePiegeonsBoss = true;

        var rend = GetComponent<Renderer>();
        rend.sharedMaterial = bossMaterial;

        _allzones = FindObjectsOfType<ZoneScript>();
        _allHunters = FindObjectsOfType<NPCHunter>();

        SearchTarget(null);
    }

    void SearchTarget(ZoneScript zone){
        
        if (_allzones.Length > 0 && _allHunters.Length > 0){
            var x = _allzones[0].countHunter;
            foreach (var element in _allzones){
                var y = element.countHunter;
                if (x >= y && zone != element){
                    _target = element;
                }
            }
        }
    }

    void Update(){
        if (_target){
            agent.SetDestination(_target.transform.position);
            var distance = Vector3.Distance(transform.position, _target.transform.position);
            if (distance < 10)
                SearchTarget(_target);
        }
    }

    void OnDestroy(){
        InfoReference.havePiegeonsBoss = false;
    }

}
