﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCHunter : NPCBase {

    public Transform positionWeapon { get; private set; }
    public LineOfSight lineOfSight { get; private set; }
    public Transform target { get; private set; }
    public Weapon currentWeapon { get; set; }
    NPCPiegon[] _allPigeons;
    public Node decisionTreeRoot;
    public int bullets;

    void Start() {
        lineOfSight = GetComponent<LineOfSight>();

        //Pigeons
        _allPigeons = FindObjectsOfType<NPCPiegon>();

        //Save the position.
        positionWeapon = GetComponentInChildren<Reference>().transform;
        
        //Add states.
        sm = new StateMachine();
        sm.AddState(new StateGetWeapon(sm, this));
        sm.AddState(new StateGetBullets(sm, this));
        sm.AddState(new StateReload(sm, this));
        sm.AddState(new StatePursuit(sm, this));
        sm.AddState(new StatePatrol(sm, this));
        sm.AddState(new StateShoot(sm, this));

        //First decision.
        decisionTreeRoot.Execute(this);
    }

    void Update() {
        sm.Update();
        EventsCharacter();
    }

    void EventsCharacter(){

        if (sm.IsActualState<StatePatrol>() || sm.IsActualState<StatePursuit>() || sm.IsActualState<StateShoot>()){
            CheckPiegons();
            lineOfSight.target = target;

            if (lineOfSight.TargetInSight && !sm.IsActualState<StatePursuit>())
                decisionTreeRoot.Execute(this);
            
        }

        if (currentWeapon){
            if (bullets <= 0 &&  currentWeapon.ammo <= 0 && sm.IsActualState<StateGetBullets>())
                decisionTreeRoot.Execute(this);
            else{
                if (currentWeapon.loadedBullet){
                    if (!sm.IsActualState<StatePatrol>()){
                        decisionTreeRoot.Execute(this);
                    }
                }
            }
        }
    }

    void CheckPiegons(){
        //Pigeons
        _allPigeons = FindObjectsOfType<NPCPiegon>();

        if (_allPigeons.Length > 0){
            var x = Vector3.Distance(transform.position, _allPigeons[0].transform.position);
            target = _allPigeons[0].transform;

            foreach (var element in _allPigeons){
                var y = Vector3.Distance(transform.position, element.transform.position);

                if (x > y)
                {
                    x = y;
                    target = element.transform;
                }
            }
        }
    }

    void OnTriggerEnter(Collider c){

        if (!currentWeapon && c.gameObject.layer == 8){//Catch weapon and bullets.
            c.gameObject.GetComponent<ICollectable>().Grab(this);
            decisionTreeRoot.Execute(this);//Decision.
        }
        if (c.gameObject.layer == 9){//More bullets.
            c.gameObject.GetComponent<ICollectable>().Grab(this);
            decisionTreeRoot.Execute(this);//Decision.
        }
    }

    public void AddBullets(int b){ bullets += b; }
    public void RemoveBullets(int b) { bullets -= b; }
}
