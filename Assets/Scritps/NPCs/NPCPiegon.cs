﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCPiegon : MonoBehaviour {

    NavMeshAgent agent;
    public static GameObject target;

    void Start () {

        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        agent.stoppingDistance = 3;
        agent.speed = 5;
        agent.acceleration = 15;
    }
	
	// Update is called once per frame
	void Update () {
        if (target){
            agent.SetDestination(target.transform.position);
        }
    }

    public void OnDestroy(){
        Destroy(gameObject);
    }
}
