﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    float _timeLife = 5;
    float _speed = 10;
    float rotationSpeed = 5;
    private Vector3 _dirToGo;
    public Transform target;

    void Update(){
        Think();
        _timeLife -= Time.deltaTime;
        if (_timeLife <= 0)
            Destroy(gameObject);
    }

    protected void Think(){
        
        if (target){
            _dirToGo = target.transform.position - transform.position;
            transform.forward = Vector3.Lerp(transform.forward, _dirToGo, rotationSpeed * Time.deltaTime);
            transform.position += transform.forward * _speed * Time.deltaTime;
        }else{
            transform.Translate(Vector3.forward * Time.deltaTime * _speed);
        }
    }

    protected void OnHit(){
        Debug.Log("BulletBase Hit");
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider c){

        if (c.gameObject.layer == 11){
            var x = c.gameObject.GetComponent<NPCPiegon>();
            x.OnDestroy();
            OnHit();
        }
    }

    void OnDrawGizmos(){
        Gizmos.color = Color.red;

        Gizmos.DrawLine(transform.position, transform.position + transform.forward * _speed);
        Gizmos.color = Color.white;

        Gizmos.DrawLine(transform.position, transform.position + _dirToGo.normalized * _speed);
    }
}
