﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneScript : MonoBehaviour {

    public int countHunter { get; private set; }

    void OnTriggerEnter(Collider c){
        if (c.gameObject.layer == 10)
            countHunter++;
    }

    void OnTriggerExit(Collider c){
        if (c.gameObject.layer == 10)
            countHunter--;
    }
}
