﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackBullets : MonoBehaviour, ICollectable {

    public int bullets;

    public void Grab(NPCHunter npc) {
        npc.AddBullets(bullets);
        Destroy(gameObject);
    }
}
