﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour, ICollectable {

    NPCHunter _npc;
    BoxCollider _collider;
    public GameObject BulletType;
    public int ammo { get; private set; }
    public int bullets = 0;
    public bool haveBoss { get; private set; }
    public bool loadedBullet { get; private set; }
    bool isShooting = false;
    float _currentTimeWaitForShoot;
    public float timeWaitForShoot = 2;

    void Start(){
        _currentTimeWaitForShoot = timeWaitForShoot; 
    }

    public void Grab(NPCHunter npc){
        _npc = npc;
        _npc.currentWeapon = this;
        npc.AddBullets(bullets);
        _collider = this.gameObject.GetComponent<BoxCollider>();
        _collider.enabled = false;
        haveBoss = true;
    }

    //Reload one bullet.
    public void ReloadWeapon(int num){
        if (!loadedBullet){
            _npc.RemoveBullets(num);
            ammo = num;
            loadedBullet = true;
        }
    }

    public void Shoot(Transform target){
        if (loadedBullet && !isShooting){
            isShooting = true;
            var bullet = Instantiate(BulletType);
            bullet.transform.rotation = transform.rotation;
            bullet.transform.position = GetComponentInChildren<Reference>().transform.position + transform.forward * 1.1f;
            var x = bullet.GetComponent<Bullet>();
            x.target = target;
            ammo--;
        }
    }

    void Update(){
        if (isShooting){
            _currentTimeWaitForShoot -= Time.deltaTime;
            if (_currentTimeWaitForShoot <= 0){
                isShooting = false;
                _currentTimeWaitForShoot = timeWaitForShoot;
            }
        }

        if (_npc != null){
            transform.position = _npc.positionWeapon.transform.position;
            transform.rotation = _npc.positionWeapon.transform.rotation;
        }
        if (ammo <= 0){
            loadedBullet = false;
        }
    }
}
