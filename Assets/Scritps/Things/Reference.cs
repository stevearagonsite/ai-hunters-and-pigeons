﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reference : MonoBehaviour {

    float _lineSize = 1f;
    float _cubeSize = 0.2f;

    void OnDrawGizmos(){
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, new Vector3(_cubeSize, _cubeSize, _cubeSize));
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * _lineSize);
    }
}
